<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/pattern.css" />

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/lib/Base64.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/lib/Base64binary.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/lib/MIDI.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/midi_loader.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/model/chord.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/model/chord_member.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/player/chord_player.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/components/chord_block.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/player/pattern_player.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/components/pattern_editor.js" defer></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/pattern_creation.js" defer></script>

<div class="pattern-properties-panel panel">
    <h3>Pattern Properties</h3>
    <s:form action="savepattern">
        <s:textfield label="Pattern Title" value="%{title}" required="true" />
        <s:hidden name="pattenData" value="" />
        <%--<s:hidden name="octave" value="" />--%>
        <%--<s:hidden name="tempo" value="" />--%>
        <%--<s:hidden name="arpDelay" value="" />--%>
        <s:submit value="Save Pattern" />
    </s:form>

    <div id="patternEditor" class="pattern-editor">Click ADD to add chord.</div>
    <button id="addChord">Add Chord</button>
    <button id="insertChordBefore" hidden>Insert Before</button>
    <button id="insertChordAfter" hidden>Insert After</button>
    <button id="deleteChord" hidden>Delete</button>
    <br />
    <button id="playPattern">Play Pattern</button>
    <button id="stopPattern">Stop Pattern</button>
</div>

<div class="chord-select-panel panel">
    <h3>Chord Select</h3>
    <s:select id="rootSelect" label="Root" list="creatorData.roots" listKey="code" listValue="label" value="3" />
    <s:select id="chordTypeSelect" label="Chord Type" list="creatorData.chordTypes" listKey="valueMap" listValue="label" />
    <button id="playChord">Play Chord</button>
</div>

<div class="global-properties-panel panel">
    <h3>Global Properties</h3>
    <s:select id="octaveSelect" label="Octave" list="creatorData.octaves" value="3" />
    <label for="tempoField">Tempo:</label>
    <input id="tempoField" name="tempo" type="number" min="60" max="200" step="1" value="120" />
    <label for="delayField">Arp Delay:</label>
    <input type="number" id="delayField" value="3" step="1" min="0" max="10" />
</div>