function ChordPlayer () {

    this.playChord = function(chord, octave, arpDelay, tempo, length, baseDelay) {
        length = length || 4;
        baseDelay = baseDelay || 0;
        
        var members = chord.getMembers();
        var noteOffDelay = baseDelay + (60.0 / tempo * length);
        for (var i = 0; i < members.length; i++) {
            var noteCode = members[i] + (12 * octave);
            setNoteOn(noteCode, baseDelay + (arpDelay * i) / 50);
            setNoteOff(noteCode, noteOffDelay);
        }
    };
    
    //private 
    function setNoteOn(noteCode, delay) {
        var velocity = 127;
        MIDI.setVolume(0, 127);
        MIDI.noteOn(0, noteCode, velocity, delay);
    }

    function setNoteOff(noteCode, delay) {
        MIDI.noteOff(0, noteCode, delay);
    }
}
