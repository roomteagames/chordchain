/**
 * Created by russelltemplet on 9/25/16.
 */

function Chord (_title, _rootCode, _members) {
    var BASE = 21;

    var title = _title;
    var members = getChordMembers();
    
    function getChordMembers() {
        var members = [];
        for (var i = 0; i < _members.length; i++) {
            //todo: move this logic to player
            // var note = BASE + _rootCode + _members[i] + (12 * _octave);
            var note = BASE + _rootCode + _members[i];
            members.push(note);
        }
        return members;
    }

    this.getTitle = function() {
        return title;
    };

    this.getMembers = function() {
        return members;
    };

    this.toJSON = function() {
        return {
            title: title,
            members: members
        };
    };

}