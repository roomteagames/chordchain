<%@ taglib prefix="s" uri="/struts-tags" %>

<s:form action="login" validate="true">
    <s:textfield label="Username" name="user.userId" required="true" />
    <s:password label="Password" name="user.password" required="true" />
    <s:submit />
</s:form>