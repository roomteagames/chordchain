<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="header-text horizontal-arrangement">Chord Chain</div>

<div class="horizontal-arrangement right-aligned">
    <s:if test="#session['user'] != null">
        <a href="logout.action">Logout</a>
    </s:if>
    <s:else>
        <a href="loginpage.action">Login</a>
    </s:else>
</div>