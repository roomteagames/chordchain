<%@ taglib prefix="s" uri="/struts-tags"%>

<s:form action="newpattern">
    <s:textfield label="Enter pattern title" name="title" required="true" />
    <s:submit value="Create Pattern" />
</s:form>

<s:if test="%{patternList}">
    <s:form action="loadpattern">
        <s:select list="%{patternList}" label="Select Pattern" />
        <s:submit value="Load Pattern" />
    </s:form>
</s:if>
<s:else>
    <p>You have no saved patterns. Create a new one!</p>
</s:else>