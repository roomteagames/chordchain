<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
    <link href="${pageContext.request.contextPath}/stylesheets/main.css" type="text/css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <title>
        <tiles:insertAttribute name="title" ignore="true" />
    </title>
</head>
<body>
    <div class="header dark-theme">
        <tiles:insertAttribute name="header" />
    </div>
    <div class="content-pane">
        <div class="menu content horizontal-arrangement">
            <tiles:insertAttribute name="menu" />
        </div>
        <div class="body content horizontal-arrangement">
            <tiles:insertAttribute name="body" />
        </div>
    </div>
    <div class="footer dark-theme">
        <tiles:insertAttribute name="footer" />
    </div>
</body>
</html>