/**
 * Created by russelltemplet on 9/25/16.
 */

(function() {
    
    // Chord select controls
    var chordControls = $('#chordControls');
    var rootSelect = $('#rootSelect');
    var chordTypeSelect = $('#chordTypeSelect');
    var octaveSelect = $('#octaveSelect');
    var delayField = $('#delayField');
    var playChordButton = $('#playChord');
    
    // Pattern edit controls
    var addChordButton = $('#addChord');
    var insertChordAfterButton = $('#insertChordAfter');
    var insertChordBeforeButton = $('#insertChordBefore');
    var deleteChordButton = $('#deleteChord');
    var playPatternButton = $('#playPattern');
    var stopPatternButton = $('#stopPattern');

    // Pattern properties
    var tempoField = $('#tempoField');

    // Service objects
    var midiLoader = new MidiLoader([playChordButton]);
    var chordPlayer = new ChordPlayer();
    var patternPlayer = new PatternPlayer();
    var patternEditor = new PatternEditor(document.getElementById('patternEditor'), onSelectChord, onDeselectChord);   //TODO: change to JQuery

    function onPlayChord () {
        var chord = getSelectedChord();
        var globalData = getGlobalData();
        chordPlayer.playChord(chord, globalData.octave, globalData.delay, globalData.tempo);
    }

    function onPlayPattern () {
        var pattern = patternEditor.getChordBlocks();
        var globalData = getGlobalData();
        patternPlayer.playPattern(pattern, globalData.octave, globalData.delay, globalData.tempo);
    }

    function onStopPattern () {
        patternPlayer.stopPattern();
    }
    
    function onAddChord() {
        var chord = getSelectedChord();
        patternEditor.addChord(chord);
    }

    function onInsertBefore() {
        var chord = getSelectedChord();
        patternEditor.insertChordBefore(chord);
    }

    function onInsertAfter() {
        var chord = getSelectedChord();
        patternEditor.insertChordAfter(chord);
    }
    
    function onSelectChord() {
        insertChordAfterButton.show();
        insertChordBeforeButton.show();
        deleteChordButton.show();
    }
    
    function onDeselectChord() {
        insertChordAfterButton.hide();
        insertChordBeforeButton.hide();
        deleteChordButton.hide();
    }
    

    function getSelectedChord() {
        var rootCode = parseInt(rootSelect.val());
        var rootLabel = rootSelect.find('option:selected').text();
        var chordTypeMap = JSON.parse(chordTypeSelect.val());

        return new Chord(
            rootLabel + chordTypeMap.symbol,
            rootCode,
            chordTypeMap.members
        );
    }

    function getGlobalData() {
        var octave = parseInt(octaveSelect.val());
        var delay = parseInt(delayField.val());
        var tempo = parseInt(tempoField.val());

        return {
            octave: octave,
            tempo: tempo,
            delay: delay
        };
    }

    midiLoader.load();

    playChordButton.click(onPlayChord);
    playPatternButton.click(onPlayPattern);
    stopPatternButton.click(onStopPattern);
    
    addChordButton.click(onAddChord);
    insertChordAfterButton.click(onInsertAfter);
    insertChordBeforeButton.click(onInsertBefore);
    deleteChordButton.click(patternEditor.deleteSelectedChord);

    $(document).on('keypress', function(e) {
        if (e.keyCode == $.ui.keyCode.ENTER) {
            onPlayChord();
        }
    });
})();