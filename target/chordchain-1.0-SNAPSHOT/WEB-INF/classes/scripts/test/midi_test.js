/**
 * Created by russelltemplet on 9/22/16.
 */

window.onload = function() {
    var button1 = $('#playSound1')[0];
    var button2 = $('#playSound2')[0];
    MIDI.loadPlugin({
        instrument: "piano",
        onsuccess: function() {
            button1.disabled = false;
            button2.disabled = false;
        }
    });

    button1.onclick = function(e) {
        var note = getNoteCode('C', 3);
        playMinor7(note);
    };
    
    button2.onclick = function(e) {
        playNote(21);
    };

    function playNote(note) {
        var velocity = 127;
        MIDI.setVolume(0, 127);
        MIDI.noteOn(0, note, velocity, 0);
    }

    function playChord(root, intervals) {
        playNote(root);
        for (var i = 0; i < intervals.length; i++) {
            var interval = intervals[i];
            playNote(root + interval);
        }
    }

    function playMajor(root) {
        playChord(root, [4, 7, 12]);
    }

    function playMajor6(root) {
        playChord(root, [4, 7, 9, 12]);
    }

    function playMinor6(root) {
        playChord(root, [3, 7, 9, 12]);
    }

    function playMajor7(root) {
        playChord(root, [4, 7, 11, 12]);
    }

    function playMajor9(root) {
        playChord(root, [4, 7, 11, 12, 14]);
    }

    function playMajor11(root) {
        playChord(root, [4, 7, 11, 12, 17]);
    }

    function playMajor13(root) {
        playChord(root, [4, 7, 11, 12, 21]);
    }

    function playMinor(root) {
        playChord(root, [3, 7, 12]);
    }

    function playMinor7(root) {
        playChord(root, [3, 7, 10, 12]);
    }

    function playDiminished(root) {
        playChord(root, [3, 6, 12]);
    }

    function playAugmented(root) {
        playChord(root, [4, 8, 12]);
    }

    function getNoteCode(pitch, octave) {
        var pitchMap = {
            'A': 0,
            'B': 2,
            'C': 3,
            'D': 5,
            'E': 7,
            'F': 8,
            'G': 10
        };
        var base = 21;
        return pitchMap[pitch] + base + (12 * octave);
    }
};