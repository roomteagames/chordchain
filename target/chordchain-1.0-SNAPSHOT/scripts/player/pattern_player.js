/**
 * Created by russelltemplet on 9/27/16.
 */

function PatternPlayer() {

    var chordPlayer = new ChordPlayer();

    this.playPattern = function(pattern, octave, arpDelay, tempo) {
        var totalDelay = 0;
        for (var i = 0, len = pattern.length; i < len; i++) {
            var chordMember = pattern[i].getChordMember();
            var chordLength = chordMember.getLength();
            var chordTime = chordLength * (60.0 / tempo);
            chordPlayer.playChord(chordMember.getChord(), octave, arpDelay, tempo, chordLength, totalDelay);
            totalDelay += chordTime;
        }
    };

    this.stopPattern = function() {
        //todo: make this work
        MIDI.stopAllNotes();
    };
}