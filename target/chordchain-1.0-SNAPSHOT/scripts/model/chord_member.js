/**
 * Created by russelltemplet on 9/26/16.
 */

function ChordMember(_chord, _length) {
    var chord = _chord;
    var length = _length;
    
    this.getChord = function() {
        return chord;
    };
    
    this.getLength = function() {
        return length;
    };

    this.setLength = function(__length) {
        length = __length;
    };

    this.toJSON = function() {
        return {
            chord: chord,
            length: length
        };
    };
    
}