/**
 * Created by russelltemplet on 9/26/16.
 */

function PatternEditor (_el, _selectCallback, _deselectCallback) {

    var me = this;
    var el = _el;
    var selectCallback = _selectCallback;   //fn called when a chord is selected
    var deselectCallback = _deselectCallback;   //fn called when a chord is deselected

    var chordBlocks = [];
    var selectedChordBlock = null;
    var beatLength = 0;
    var measureLength = 4;
    var currentLength = 4;
    var measureWidth = 150;

    this.addChord = function(chord) {
        var chordBlock = createChordElement(chord);
        chordBlocks.push(chordBlock);
        onAfterAdd(chordBlock);
    };

    this.insertChordBefore = function(chord) {
        var chordBlock = createChordElement(chord);
        var selectedIndex = chordBlocks.indexOf(selectedChordBlock);
        chordBlocks.splice(selectedIndex, 0, chordBlock);
        onAfterAdd(chordBlock);
    };

    this.insertChordAfter = function(chord) {
        var chordBlock = createChordElement(chord);
        var selectedIndex = chordBlocks.indexOf(selectedChordBlock);
        if (selectedIndex < chordBlocks.length - 1) {
            chordBlocks.splice(selectedIndex + 1, 0, chordBlock);
        } else {
            chordBlocks.push(chordBlock);
        }
        onAfterAdd(chordBlock);
    };

    this.shortenSelectedChord = function() {

        beatLength--;
        updateHtml();
    };

    this.lengthenSelectedChord = function() {

        beatLength++;
        updateHtml();
    };

    this.deleteSelectedChord = function() {
        var blockIndex = chordBlocks.indexOf(selectedChordBlock);
        chordBlocks.splice(blockIndex, 1);

        subtractChordLength(selectedChordBlock);
        updateHtml();

        deselectCallback();
    };

    this.getSelectedChord = function() {
        return selectedChordBlock.chord;
    };

    this.getChordBlocks = function() {
        return chordBlocks;
    };

    this.getBeatLength = function() {
        return beatLength;
    };


    //private
    function createChordElement(chord) {
        return new ChordBlock(
            document.createElement('div'),
            new ChordMember(chord, currentLength),
            measureWidth / measureLength,
            chordMouseUpCallback
        );
    }

    function onAfterAdd(chordBlock) {
        addChordLength(chordBlock);
        updateHtml();
    }

    function addChordLength(chordEl) {
        beatLength += chordEl.getChordMember().getLength();
    }

    function subtractChordLength(chordEl) {
        beatLength -= chordEl.getChordMember().getLength();
    }

    function updateHtml() {
        destroyInnerHtml();

        var patternChild = document.createElement('div');
        patternChild.setAttribute('class', 'pattern');

        generateBackground(patternChild);
        appendChordBlocks(patternChild);

        el.appendChild(patternChild);
    }

    function generateBackground(node) {
        var numMeasures = beatLength / measureLength + 7;

        for (var i = 0; i < numMeasures; i++) {
            var measureChild = document.createElement('div');
            measureChild.setAttribute('class', 'measure');

            for (var j = 0; j < measureLength; j++) {
                var beatChild = document.createElement('div');
                beatChild.setAttribute('class', 'beat');
                measureChild.appendChild(beatChild);
            }

            node.appendChild(measureChild);
        }
    }

    function appendChordBlocks(node) {
        var beatProgress = 0;
        var beatWidth = measureWidth / measureLength;
        for (var i = 0; i < chordBlocks.length; i++) {
            var chordBlock = chordBlocks[i];
            var chordLength = chordBlock.getChordMember().getLength();
            //todo: adding i won't work unless the chord is exactly 1 measure long
            var blockLeft = beatWidth * beatProgress + i;
            
            chordBlock.applyLeft(blockLeft);
            node.appendChild(chordBlock.getNode());
            beatProgress += chordLength;
        }
    }

    function destroyInnerHtml() {
        el.removeChild(el.firstChild);
    }

    function chordMouseUpCallback(event, chordBlock) {
        if (selectedChordBlock) {
            selectedChordBlock.deselect();
        }
        chordBlock.select();
        selectedChordBlock = chordBlock;
        if (event.which == 3) {
            me.deleteSelectedChord();
        } else {
            selectCallback();
        }
    }
}