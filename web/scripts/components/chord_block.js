/**
 * Created by russelltemplet on 9/27/16.
 */

function ChordBlock (_node, _chordMember, _beatWidth, _mouseUpCallback) {
    var node = _node;
    var el = $(_node);
    var chordMember = _chordMember;
    var beatWidth = _beatWidth;
    var mouseUpCallback = _mouseUpCallback;
    var me = this;

    // public
    this.applyLength = function(length) {
        chordMember.setLength(length);
        var blockWidth = beatWidth * length - 2;
        el.css({
            'width': blockWidth
        });
    };

    this.applyLeft = function(left) {
        el.css({
            'left': left
        });
    };

    this.select = function() {
        el.addClass('selected');
    };

    this.deselect = function() {
        el.removeClass('selected');
    };

    this.toJSON = function() {
        return chordMember;
    };

    this.getChordMember = function() {
        return chordMember;
    };

    this.getNode = function() {
        return node;
    };


    // init
    (function() {
        var chordLength = chordMember.getLength();
        
        el.addClass('chord-block');
        el.text(chordMember.getChord().getTitle());
        me.applyLength(chordLength);

        el.on('contextmenu', function(e) {
            return false;
        });

        el.mouseup(function(e) {
            mouseUpCallback(e, me);
        });
    })();
}