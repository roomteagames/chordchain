/**
 * Created by russelltemplet on 9/27/16.
 */

function MidiLoader (_playerElements) {
    var playerElements = _playerElements;

    this.load = function(instrument) {
        instrument = instrument || 'piano';
        MIDI.loadPlugin({
            instrument: instrument,
            onsuccess: function() {
                playerElements.forEach(function(el) {
                    el.disabled = false;
                });
            }
        });
        playerElements.forEach(function(el) {
            el.disabled = true;
        });
    };
}