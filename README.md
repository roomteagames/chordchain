<p>Welcome to Chord Chain!</p>

<p>This site allows you to listen to chords and arrange them way you choose.
    Just select a root, chord, and press listen.
    Click into the pattern region to append that chord to your pattern.
    Click on the chord and drag left to shorten and right to lengthen the chord.
    Click the play button to hear the arrangement played in full.
    Give your pattern a title and save it for later use!</p>

<p>And best of all... it's FREE!</p>
