package org.tearoom.chordchain.factory;

import org.tearoom.chordchain.model.ChordType;
import org.tearoom.chordchain.model.CreatorData;
import org.tearoom.chordchain.model.Pitch;

import java.util.Arrays;
import java.util.List;

/**
 * Created by russelltemplet on 9/25/16.
 */
public class CreatorDataFactory {

    private static final List<Pitch> pitches = Arrays.asList(
            new Pitch("Ab", -1),
            new Pitch("A", 0),
            new Pitch("A#", 1),
            new Pitch("Bb", 1),
            new Pitch("B", 2),
            new Pitch("C", 3),
            new Pitch("C#", 4),
            new Pitch("Db", 4),
            new Pitch("D", 5),
            new Pitch("D#", 6),
            new Pitch("Eb", 6),
            new Pitch("E", 7),
            new Pitch("F", 8),
            new Pitch("F#", 9),
            new Pitch("Gb", 9),
            new Pitch("G", 10),
            new Pitch("G#", 11)
    );
    private static final List<ChordType> chordTypes = Arrays.asList(
            new ChordType(Arrays.asList(0, 4, 7, 12), "", "Major"),
            new ChordType(Arrays.asList(0, 4, 7, 9, 12), "6", "Major 6th"),
            new ChordType(Arrays.asList(0, 4, 7, 10, 12), "7", "Dominant 7th"),
            new ChordType(Arrays.asList(0, 4, 7, 11, 12), "M7", "Major 7th"),
            new ChordType(Arrays.asList(0, 4, 7, 11, 12, 14), "9", "Major 9th"),
            new ChordType(Arrays.asList(0, 4, 7, 9, 14), "6/9", "Major 6th Over 9th"),
            new ChordType(Arrays.asList(0, 4, 5, 7, 11, 12), "11", "Major 11th"),
            new ChordType(Arrays.asList(0, 7, 9, 11, 14), "13", "Major 13th"),
            new ChordType(Arrays.asList(0, 3, 7, 12), "m", "Minor"),
            new ChordType(Arrays.asList(0, 3, 7, 9, 12), "m6", "Minor 6th"),
            new ChordType(Arrays.asList(0, 3, 7, 10, 12), "m7", "Minor 7th"),
            new ChordType(Arrays.asList(0, 3, 7, 11, 12), "mM7", "Minor Major 7th"),
            new ChordType(Arrays.asList(0, 3, 6, 12), "dim", "Diminished"),
            new ChordType(Arrays.asList(0, 3, 6, 9, 12), "dim7", "Diminished 7th"),
            new ChordType(Arrays.asList(0, 3, 6, 10, 12), "hdim7", "Half-Diminished 7th"),
            new ChordType(Arrays.asList(0, 4, 8, 12), "aug", "Augmented"),
            new ChordType(Arrays.asList(0, 4, 8, 10, 12), "aug7", "Augmented Minor 7th"),
            new ChordType(Arrays.asList(0, 4, 8, 11, 12), "augM7", "Augmented Major 7th"),
            new ChordType(Arrays.asList(0, 2, 7, 12), "sus2", "Sustained 2nd"),
            new ChordType(Arrays.asList(0, 2, 7, 10, 12), "sus2/7", "Sustained 2nd 7th"),
            new ChordType(Arrays.asList(0, 5, 7, 12), "sus4", "Sustained 4th"),
            new ChordType(Arrays.asList(0, 5, 7, 10, 12), "sus4/7", "Sustained 4th 7th")
    );
    private static final List<Integer> octaves = Arrays.asList(1, 2, 3, 4, 5);

    public CreatorData getCreatorData() {
        return new CreatorData(pitches, chordTypes, octaves);
    }

    public List<Pitch> getRoots() {
        return pitches;
    }

    public List<ChordType> getChordTypes() {
        return chordTypes;
    }

    public List<Integer> getOctaves() {
        return octaves;
    }
}
