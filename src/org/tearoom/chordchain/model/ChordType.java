package org.tearoom.chordchain.model;

import flexjson.JSONSerializer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by russelltemplet on 9/22/16.
 */
public class ChordType {

    private Map<String, Object> valueMap;
    private List<Integer> members;
    private String symbol;
    private String label;

    public ChordType() {}

    public ChordType(List<Integer> _halfSteps, String _symbol, String _label) {
        members = _halfSteps;
        symbol = _symbol;
        label = _label;

        valueMap = new HashMap<String, Object>() {
            @Override
            public String toString() {
                return new JSONSerializer().deepSerialize(this);
            }
        };
        valueMap.put("members", members);
        valueMap.put("symbol", symbol);
    }

    public List<Integer> getMembers() {
        return members;
    }

    public void setMembers(List<Integer> members) {
        this.members = members;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Map<String, Object> getValueMap() {
        return valueMap;
    }

    public void setValueMap(Map<String, Object> valueMap) {
        this.valueMap = valueMap;
    }
}
