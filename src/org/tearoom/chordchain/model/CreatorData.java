package org.tearoom.chordchain.model;

import java.util.List;

/**
 * Created by russelltemplet on 9/25/16.
 */
public class CreatorData {

    private List<Pitch> roots;
    private List<ChordType> chordTypes;
    private List<Integer> octaves;

    public CreatorData () {}

    public CreatorData (List<Pitch> _roots, List<ChordType> _chordTypes, List<Integer> _octaves) {
        roots = _roots;
        chordTypes = _chordTypes;
        octaves = _octaves;
    }

    public List<Pitch> getRoots() {
        return roots;
    }

    public void setRoots(List<Pitch> roots) {
        this.roots = roots;
    }

    public List<ChordType> getChordTypes() {
        return chordTypes;
    }

    public void setChordTypes(List<ChordType> chordTypes) {
        this.chordTypes = chordTypes;
    }

    public List<Integer> getOctaves() {
        return octaves;
    }

    public void setOctaves(List<Integer> octaves) {
        this.octaves = octaves;
    }
}
