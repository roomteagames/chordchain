package org.tearoom.chordchain.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class Chord {

    private String title;
    private List<Integer> notes;

    public Chord() {}

    public Chord(String _title, List<Integer> _notes) {
        title = _title;
        notes = _notes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Integer> getNotes() {
        return notes;
    }

    public void setNotes(List<Integer> notes) {
        this.notes = notes;
    }

    public String toString() {
        return title;
    }
}
