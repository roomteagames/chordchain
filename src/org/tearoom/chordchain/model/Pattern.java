package org.tearoom.chordchain.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class Pattern {

    private String title;
    private List<ChordMember> chords;

    public Pattern() {}

    public Pattern(String _title, List<ChordMember> _chords) {
        title = _title;
        chords = _chords;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ChordMember> getChords() {
        return chords;
    }

    public void setChords(List<ChordMember> chords) {
        this.chords = chords;
    }
}
