package org.tearoom.chordchain.model;

import java.io.Serializable;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class ChordMember {

    private Chord chord;
    private int duration;

    public ChordMember() {}

    public ChordMember(Chord _chord, int _duration) {
        chord = _chord;
        duration = _duration;
    }

    public Chord getChord() {
        return chord;
    }

    public void setChord(Chord chord) {
        this.chord = chord;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
