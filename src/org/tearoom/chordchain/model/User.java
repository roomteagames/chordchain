package org.tearoom.chordchain.model;

import java.io.Serializable;

/**
 * Created by russelltemplet on 9/20/16.
 */
public class User {

    private String userId;
    private String password;

    public User() {}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
