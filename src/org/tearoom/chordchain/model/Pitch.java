package org.tearoom.chordchain.model;

/**
 * Created by russelltemplet on 9/25/16.
 */
public class Pitch {

    private String label;
    private int code;

    public Pitch() {}

    public Pitch(String _pitch, int _code) {
        label = _pitch;
        code = _code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
