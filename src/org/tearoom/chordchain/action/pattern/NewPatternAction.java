package org.tearoom.chordchain.action.pattern;

import com.opensymphony.xwork2.ActionSupport;
import org.tearoom.chordchain.factory.CreatorDataFactory;
import org.tearoom.chordchain.model.CreatorData;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class NewPatternAction extends ActionSupport {

    private String title;
    private CreatorData creatorData;

    @Override
    public String execute() {
        creatorData = new CreatorDataFactory().getCreatorData();
        return SUCCESS;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public CreatorData getCreatorData() {
        return creatorData;
    }

    public void setCreatorData(CreatorData creatorData) {
        this.creatorData = creatorData;
    }
}
