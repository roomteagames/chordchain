package org.tearoom.chordchain.action.pattern;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.tearoom.chordchain.model.Pattern;
import org.tearoom.chordchain.service.DashboardService;

import java.util.List;
import java.util.Map;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class DashboardAction extends ActionSupport implements SessionAware {

    private List<Pattern> patternList;
    private Map<String, Object> session;
    private DashboardService service;

    public DashboardAction () {
        //TODO: use a bean config to create this
        service = new DashboardService();
    }

    @Override
    public String execute() {
        patternList = service.loadPatterns(session);
        return SUCCESS;
    }

    public void setSession(Map<String, Object> _session) {
        session = _session;
    }
}
