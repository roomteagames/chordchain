package org.tearoom.chordchain.action.authentication;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.tearoom.chordchain.model.User;
import org.tearoom.chordchain.service.LoginService;

import java.util.Map;

/**
 * Created by russelltemplet on 9/20/16.
 */
public class LoginAction extends ActionSupport implements SessionAware {

    private User user;
    private Map<String, Object> session;
    private LoginService service;

    public LoginAction () {
        service = new LoginService();   //TODO: use a bean definition for this...
    }

    @Override
    public String execute() throws Exception {
        clearFieldErrors();
        String result = LOGIN;
        if (service.verifyLogin(user, session)) {
            result = SUCCESS;
        }
        return result;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setSession(Map<String, Object> _session) {
        session = _session;
    }
}
