package org.tearoom.chordchain.action.authentication;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;
import org.tearoom.chordchain.Constants;

import java.util.Map;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class LogoutAction extends ActionSupport implements SessionAware {

    private Map<String, Object> session;

    @Override
    public String execute() {
        session.remove(Constants.SESSION_USER_KEY);
        return SUCCESS;
    }

    public void setSession(Map<String, Object> _session) {
        session = _session;
    }
}
