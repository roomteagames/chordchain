package org.tearoom.chordchain;

import org.tearoom.chordchain.model.ChordType;
import org.tearoom.chordchain.model.Pitch;

import java.util.Arrays;
import java.util.List;

/**
 * Created by russelltemplet on 9/21/16.
 */
public final class Constants {

    // Session keys
    public static final String SESSION_USER_KEY = "user";
    public static final String SESSION_PATTERNS_KEY = "patterns";
}
