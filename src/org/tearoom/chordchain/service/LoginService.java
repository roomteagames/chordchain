package org.tearoom.chordchain.service;

import org.tearoom.chordchain.Constants;
import org.tearoom.chordchain.model.User;

import java.util.Map;

/**
 * Created by russelltemplet on 9/20/16.
 */
public class LoginService {

    public boolean verifyLogin(User user, Map<String, Object> session) {
        boolean isValid = false;

        User sessionUser = (User) session.get(Constants.SESSION_USER_KEY);
        if (sessionUser == null) {
            //TODO: validate using a database -- hibernate?
            if (user.getUserId().equals("russell") && user.getPassword().equals("password")) {
                session.put(Constants.SESSION_USER_KEY, user);
                isValid = true;
            }
        } else {
            isValid = true;
        }
        return isValid;
    }
}
