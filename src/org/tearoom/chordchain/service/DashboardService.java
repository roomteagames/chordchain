package org.tearoom.chordchain.service;

import org.tearoom.chordchain.Constants;
import org.tearoom.chordchain.model.Chord;
import org.tearoom.chordchain.model.Pattern;
import org.tearoom.chordchain.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class DashboardService {

    public List<Pattern> loadPatterns(Map<String, Object> session) {
        List<Pattern> patterns = null;
        User sessionUser = (User) session.get(Constants.SESSION_USER_KEY);
        if (sessionUser != null) {

            //TODO: load patterns for user from database
            patterns = new ArrayList<Pattern>();

            session.put(Constants.SESSION_PATTERNS_KEY, patterns);
        }
        return patterns;
    }

//    private List<Pattern> createFakePatterns () {
//        Chord chordC = new Chord("C", Arrays.asList("C", "Eb", "G"));
//        Chord chordGm = new Chord("Gm", Arrays.asList("G", ""));
//    }
}
