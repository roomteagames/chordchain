package org.tearoom.chordchain.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.interceptor.Interceptor;
import org.tearoom.chordchain.Constants;
import org.tearoom.chordchain.model.User;

import java.util.Map;

/**
 * Created by russelltemplet on 9/21/16.
 */
public class AuthenticationInterceptor implements Interceptor {

    public void destroy() {}

    public void init() {}

    public String intercept(ActionInvocation actionInvocation) throws Exception {
        Map<String, Object> session = actionInvocation.getInvocationContext().getSession();
        User user = (User) session.get(Constants.SESSION_USER_KEY);
        String result = ActionSupport.LOGIN;

        if (user != null) {
            result = actionInvocation.invoke();
        }

        return result;
    }
}
